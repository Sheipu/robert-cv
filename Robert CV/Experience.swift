//
//  Experience.swift
//  Robert CV
//
//  Created by Robert Danilovic on 2018-12-08.
//  Copyright © 2018 Robert Danilovic. All rights reserved.
//

import Foundation

struct Experience {
    var image: String
    var title: String
    var year: String
    var description: String
}
