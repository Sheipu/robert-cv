//
//  Section.swift
//  Robert CV
//
//  Created by Robert Danilovic on 2018-12-09.
//  Copyright © 2018 Robert Danilovic. All rights reserved.
//

import Foundation

class Section {
    var name: String = ""
    var rows: [Experience] = []
    
    init(name: String, rows: [Experience]) {
        self.name = name
        self.rows = rows
    }
}
