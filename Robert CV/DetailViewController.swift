//
//  DetailViewController.swift
//  Robert CV
//
//  Created by Robert Danilovic on 2018-12-08.
//  Copyright © 2018 Robert Danilovic. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var expImage: UIImageView!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet weak var descriptLbl: UILabel!
    
    
    var experience: Experience = Experience(image: "", title: "", year: "", description: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        expImage.image = UIImage(named: experience.image)
        titleLbl.text = experience.title
        yearLbl.text = experience.year
        descriptLbl.text = experience.description
    }
}
