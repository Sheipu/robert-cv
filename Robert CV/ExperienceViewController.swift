//
//  ExperienceViewController.swift
//  Robert CV
//
//  Created by Robert Danilovic on 2018-12-09.
//  Copyright © 2018 Robert Danilovic. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {
    

    @IBOutlet weak var tableView: UITableView!
    
    
    var sections: [Section] = [
        Section(name: "Experience", rows: [
        Experience(image: "work", title: "First job", year: "20xx-20xx", description: "This is how i worked."),
        Experience(image: "work", title: "Second job", year: "20xx-20xx", description: "This went like this.")
    ]),
        Section(name: "Education", rows: [
            Experience(image: "education", title: "Preschool", year: "20xx-20xx", description: "I was young."),
            Experience(image: "education", title: "Highschool", year: "20xx-20xx", description: "I was older.")
        ])
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailViewController {
            if let indexPath = sender as? IndexPath {
                let experience = self.sections[indexPath.section].rows[indexPath.row]
                destination.experience = experience
            }
        }
    }
}

extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sections[section].rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceTableViewCell", for: indexPath) as? ExperienceTableViewCell {
            cell.imageBoi.image = UIImage(named: self.sections[indexPath.section].rows[indexPath.row].image)
            cell.descLbl.text = self.sections[indexPath.section].rows[indexPath.row].description
            cell.yearLbl.text = self.sections[indexPath.section].rows[indexPath.row].year
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        headerView.backgroundColor = UIColor.black
        let headerLabel = UILabel(frame: CGRect(x: 8, y: 0, width: headerView.frame.width - 16, height: 30))
        headerLabel.textColor = UIColor.white
        headerLabel.font = UIFont.boldSystemFont(ofSize: 14)
        headerLabel.textAlignment = .center
        switch (section) {
        case 0:
            headerLabel.text = "Work"
        case 1:
            headerLabel.text = "Education"
        default:
            headerLabel.text = "Other"
        }
        headerView.addSubview(headerLabel)
        
        return headerView
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailSegue", sender: indexPath)
    }
    
    
}
