//
//  ExperienceTableViewCell.swift
//  Robert CV
//
//  Created by Robert Danilovic on 2018-12-08.
//  Copyright © 2018 Robert Danilovic. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
    @IBOutlet var imageBoi: UIImageView!
    @IBOutlet var descLbl: UILabel!
    @IBOutlet var yearLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
