//
//  ViewController.swift
//  Robert CV
//
//  Created by Robert Danilovic on 2018-12-08.
//  Copyright © 2018 Robert Danilovic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var animationView: UIView!
    
    @IBOutlet weak var width: NSLayoutConstraint!
    @IBOutlet weak var heigth: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let animateView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        animateView.center = self.view.center
        animateView.backgroundColor = UIColor.black
        
        view.addSubview(animateView)
        
        UIView.animate(withDuration: 5) {
            animateView.frame.size = CGSize(width: animateView.frame.width + 100, height: animateView.frame.width + 100)
            
            animateView.center = CGPoint(x: self.view.center.x,
                                         y: self.view.center.y)
            animateView.backgroundColor = UIColor.gray
        }
    }
}
